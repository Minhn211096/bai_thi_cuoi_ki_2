#include <stdio.h>
#include <mem.h>


int main() {

    char str[50];
    int p, b;
    int numa, nume, numi, numo, numu, num;
    int a, e, i, o, u, rest;
    printf("Enter string:");
    gets(str);

    numa = 0;
    nume = 0;
    numi = 0;
    numo = 0;
    numu = 0;
    num = 0;
    b = strlen(str);

    for (p = 0; p < b; p++) {
        if (str[p] == 'a') {
            numa++;
        } else if (str[p] == 'e') {
            nume++;
        } else if (str[p] == 'i') {
            numi++;
        } else if (str[p] == 'o') {
            numo++;
        } else if (str[p] == 'u') {
            numu++;
        } else {
            num++;
        }
    }

    a = 100 * numa / b;
    e = 100 * nume / b;
    i = 100 * numi / b;
    o = 100 * numo / b;
    u = 100 * numu / b;
    rest = 100 * num / b;

    printf("\n Numbers of characters:");
    printf("\n a: %d; e: %d;i: %d;o: %d;u: %d, rest: %d", numa, nume, numi, numo, numu, num);
    printf("\n\nPercentages of total:");
    printf("\n a: %d%%; e: %d%%;i: %d%%;o: %d%%;u: %d%%, rest: %d%%", a, e, i, o, u, rest);

    return 0;
}
